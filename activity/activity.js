const express = require("express")

const mongoose = require("mongoose")

const app = express()
const port = 4001

mongoose.connect(`mongodb+srv://admin123:admin123@cluster0.2myj4l1.mongodb.net/S35-Activity?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', console.error.bind(console, "Connection"))
db.once('open', () => console.log('Connection to MongoDB'))

const userSchema = new mongoose.Schema({
	name: String,
	password: String
})

const user = mongoose.model('User', userSchema)

app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.post('/signup', (req,res) => {
	user.findOne({name: req.body.name}, (error, result) => {
		if(error){
			return res.send(error)
		} else if(result != null && result.name == req.body.name) {
			return res.send('Duplicate user found')
		} else {
			let newUser = new user({
				name: req.body.name
				password: req.body.password
			})

			newUser.save((error, registeredUser) => {
				if(error) {
					return console.error(error)
				} else {
					return res.status(201).send('New User Created!')
				}
			})
		}
	})
})


app.listen(port, () => console.log(`Server is running at ${port}`))